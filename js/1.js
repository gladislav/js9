// 1.Щоб створити новий HTML тег на сторінці, можна використати метод document.createElement()
// 2.Функція insertAdjacentHTML() дозволяє вставити HTML-код в певне місце відносно елемента, на якому 
// вона викликається.
// 3.Щоб видалити елемент зі сторінки, використовуйте метод remove()

function elementsList(array, parentUl = document.body) {
  let ul = document.createElement("ul");
  array.forEach((element) => {
    let li = document.createElement("li");
    li.append(element);
    ul.append(li);
  });
  parentUl.prepend(ul);
}

elementsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
